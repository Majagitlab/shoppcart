import { Component, OnInit } from '@angular/core';

import { ProductService } from '../../../services/product.service';
import { Product } from '../../../models/product';
import { WishlistService } from 'src/app/services/wishlist.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {

  productList: Product[] = []
  wishlist: number[] = []
  constructor(private productService: ProductService, private wishlistService: WishlistService) { }

  ngOnInit() {
    this.loadProducts();
    this.loadWishlist();
  }


  loadProducts() {
    this.productService.getProducts().subscribe((products) => {
      this.productList = products;
    });
  }

  loadWishlist() {
    this.wishlistService.getWishlist().subscribe(productIds => {
      this.wishlist = productIds
      console.log(this.wishlist);
    })
  }
}
