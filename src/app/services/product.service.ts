import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Product } from '../models/product';
import { productsUrl } from 'src/app/config/api';


@Injectable({
  providedIn: 'root'
})
export class ProductService {

  // products: Product[] = [
  //   new Product(1, 'Product 1', 'This is the col this.product...', 100),
  //   new Product(2, 'Product 2', 'This is the col this.product...', 150),
  //   new Product(3, 'Product 3', 'This is the col this.product...', 200),
  //   new Product(4, 'Product 4', 'This is the col this.product...', 120),
  //   new Product(5, 'Product 5', 'This is the col this.product...', 150),
  //   new Product(6, 'Product 6', 'This is the col this.product...', 220),
  //   new Product(7, 'Product 7', 'This is the col this.product...', 130),
  // ]

  constructor(private http: HttpClient) { }

  getProducts(): Observable<Product[]> {
    return this.http.get<Product[]>(productsUrl);
  }
}
