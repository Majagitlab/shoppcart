import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

import { wishListUrl } from '../config/api';

@Injectable({
  providedIn: 'root'
})
export class WishlistService {

  constructor(private http: HttpClient) { }

  getWishlist() {
    return this.http.get(wishListUrl).pipe(
      map((result: any[]) => {
        let productIds = []
        result.forEach(item => productIds.push(item.id))
        return productIds;
      })
    )
  }

  addToWishlist(productId) {
    return this.http.post(wishListUrl, { id: productId })
  }

  removeFromWishlist(productId) {
    return this.http.delete(wishListUrl + '/' + productId);
  }
}
